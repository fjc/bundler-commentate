# Bundler::Commentate

Append gem summaries to a Gemfile.

## Installation

Add this line to your application's Gemfile:

```ruby
plugin 'bundler-commentate'
```

And then execute:

    $ bundle install

Or install it yourself as:

    $ gem install bundler-commentate

## Usage

```
$ bundle commentate -h
commentate [OPTIONS]
    -i, --in-place                   Overwrite Gemfile
    -g, --gemfile GEMFILE            Gemfile input,  "-" for STDIN;  default is "Gemfile"
    -o, --outfile GEMFILE            Gemfile output, "-" for STDOUT; default is STDOUT
    -v, --[no-]verbose               Run verbosely
```

### Example

Read './Gemfile' and print annotated version to STDOUT:  `bundle commentate`

```
$ cat Gemfile
# frozen_string_literal: true
source 'https://rubygems.org'
# Specify your gem's dependencies in bundler-commentate.gemspec
gemspec
gem 'rake', '~> 13.0'
gem 'minitest', '~> 5.0'
gem 'rubocop', '~> 1.7'
gem 'rubocop-minitest'
gem 'rubocop-rake'
plugin 'bundler-commentate', path: '.'

$ bundle commentate
# frozen_string_literal: true
source 'https://rubygems.org'
# Specify your gem's dependencies in bundler-commentate.gemspec
gemspec
gem "rake", "~> 13.0"   # "Rake is a Make-like program implemented in Ruby"
gem "minitest", "~> 5.0"        # "minitest provides a complete suite of testing facilities supporting TDD, BDD, mocking, and benchmarking"
gem "rubocop", "~> 1.7" # "Automatic Ruby code style checking tool."
gem "rubocop-minitest"  # "Automatic Minitest code style checking tool."
gem "rubocop-rake"      # "A RuboCop plugin for Rake"
plugin 'bundler-commentate', path: '.'
```

## Development

After checking out the repo, run `bin/setup` to install
dependencies. Then, run `rake test` to run the tests. You can also run
`bin/console` for an interactive prompt that will allow you to experiment.

To install this gem onto your local machine, run `bundle exec rake
install`. To release a new version, update the version number in
`version.rb`, and then run `bundle exec rake release`, which will create
a git tag for the version, push git commits and the created tag, and
push the `.gem` file to [rubygems.org](https://rubygems.org).

## Contributing

Bug reports and pull requests are welcome on GitHub at
https://gitlab.com/fjc/bundler-commentate.

## License

The gem is available as open source under the terms of the [MIT
License](https://opensource.org/licenses/MIT).
