# frozen_string_literal: true

$LOAD_PATH.unshift File.expand_path('../lib', __dir__)
require 'bundler/commentate'

require 'minitest/autorun'

require 'stringio'
