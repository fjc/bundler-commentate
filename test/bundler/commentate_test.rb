# frozen_string_literal: true

require 'test_helper'

class Bundler::CommentateTest < Minitest::Test
  def test_that_it_has_a_version_number
    refute_nil ::Bundler::Commentate::VERSION
  end

  def test__io_for__default_to_gemfile
    io   = Object.allocate
    mode = Object.allocate
    file = MiniTest::Mock.new
    file.expect(:call, io, ['Gemfile', mode])

    File.stub(:new, file) do
      assert_equal io, Bundler::Commentate.allocate.send(:_io_for, nil, mode)
    end

    file.verify
  end

  def test__io_for__with_named_file
    io   = Object.allocate
    mode = Object.allocate
    name = String.allocate
    file = MiniTest::Mock.new
    file.expect(:call, io, [name, mode])

    File.stub(:new, file) do
      assert_equal io, Bundler::Commentate.allocate.send(:_io_for, name, mode)
    end

    file.verify
  end

  def test__io_for__stdio
    mode  = Object.allocate
    stdio = Bundler::Commentate.allocate.send(:_io_for, '-', mode)
    assert stdio.respond_to?(:read)
    assert stdio.respond_to?(:print)
    assert stdio.respond_to?(:truncate)

    data  = Object.allocate
    stdin = MiniTest::Mock.new
    stdin.expect(:call, data)
    $stdin.stub(:read, stdin) do
      assert data, stdio.read
    end
    stdin.verify

    data   = Object.allocate
    stdout = MiniTest::Mock.new
    stdout.expect(:call, nil, [data])
    $stdout.stub(:print, stdout) do
      assert data, stdio.print(data)
    end
    stdout.verify
  end

  def test__commentate
    original_gemfile = <<~GEMFILE
      # frozen_string_literal: true
      source 'https://rubygems.org'
      # Specify your gem's dependencies in bundler-commentate.gemspec
      gemspec
      gem 'rake', '~> 13.0'
      gem 'minitest', "~> 5.0"
      gem "rubocop", '~> 1.7'
      gem 'rubocop-minitest', extra: args
      gem 'rubocop-rake'  # old comment
      plugin 'bundler-commentate', path: '.'
    GEMFILE
    commentated_gemfile = <<~GEMFILE
      # frozen_string_literal: true
      source 'https://rubygems.org'
      # Specify your gem's dependencies in bundler-commentate.gemspec
      gemspec
      gem("rake", "~> 13.0")	# "Rake is a Make-like program implemented in Ruby"
      gem("minitest", "~> 5.0")	# "minitest provides a complete suite of testing facilities supporting TDD, BDD, mocking, and benchmarking"
      gem("rubocop", "~> 1.7")	# "Automatic Ruby code style checking tool."
      gem("rubocop-minitest", { extra: args })	# "Automatic Minitest code style checking tool."
      gem("rubocop-rake")	# "A RuboCop plugin for Rake"
      plugin 'bundler-commentate', path: '.'
    GEMFILE

    input  = StringIO.new(original_gemfile)
    output = StringIO.new

    Bundler::Commentate.allocate.send(:_commentate, input, output)

    output.rewind
    assert_equal output.read, commentated_gemfile
  end
end
