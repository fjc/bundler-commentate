# frozen_string_literal: true

module Bundler
  class Commentate < Bundler::Plugin::API
    VERSION = '0.3.0'
  end
end
