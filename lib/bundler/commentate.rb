# frozen_string_literal: true

require_relative 'commentate/version'

require 'optparse'

require 'parser/current'
require 'unparser'

module Bundler
  # Add gem summaries to a Gemfile.
  class Commentate < Bundler::Plugin::API
    command 'commentate'

    def exec(command, args)
      send command, args
    end

    private

    def commentate(args)
      OptionParser.new { |opts|
        opts.banner = 'commentate [OPTIONS] [GEM, GEM, ...]'

        opts.on('-i'         , '--in-place' , 'Overwrite Gemfile'                                   )
        opts.on('-g GEMFILE' , '--gemfile'  , 'Gemfile input,  "-" for STDIN;  default is "Gemfile"')
        opts.on('-o GEMFILE' , '--outfile'  , 'Gemfile output, "-" for STDOUT; default is STDOUT'   )
      }.parse!(args, into: (opts={}))

      raise OptionParser::ParseError.new('Conflicting options: in-place, output') if opts[:'in-place'] and opts[:output]

      input  = _io_for(opts[:gemfile], 'r')
      output = opts[:'in-place'] ? _io_for(opts[:gemfile]) : opts[:outfile] ? _io_for(opts[:outfile]) : _io_for('-')

      _commentate(input, output)
    rescue
      raise Bundler::BundlerError.new($!)
    end

    # get IO for a file or stdio, default to r/w (append, create)
    def _io_for(opt, mode='a+')
      case opt
      when nil
        File.new('Gemfile', mode)
      when '-'
        Module.new do
          def self.read(...);  $stdin .read(...); end
          def self.print(...); $stdout.print(...); end
          def self.truncate(...); end
        end
      when String
        File.new(opt, mode)
      end
    end

    def _commentate(input, output)
      # "Weirich convention"
      # Use { } for blocks that return values
      # Use do / end for blocks that are executed for side effects
      input.read.tap do output.truncate(0) end.lines.each { |line|
        parsed_line = Parser::CurrentRuby.parse(line)
        unparsed_line = Unparser.unparse(parsed_line)

        case parsed_line.to_a
        in [nil, :gem, gem, *_rest]
          output.print _commentate_line(gem, unparsed_line)
        else
          output.print line
        end
      }
    end

    def _commentate_line(gem, line)
      g = gem.deconstruct[1]
      c = Gem.latest_spec_for(g).summary
      "#{line.chomp}\t# #{c.inspect}\n"
    end
  end
end
